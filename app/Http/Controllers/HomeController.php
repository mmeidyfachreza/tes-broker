<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Webhook;

class HomeController extends Controller
{
    protected $status_code = 200;

    public function issueCertificate(Request $request)
    {
        // return redirect('certificate', 307); // 200 OK karena method POST otomatis kebawa

        // return redirect()->to('certificate', 303); // 405 Method Not Allowed karena method POST tidak ikut terbawa


        // success
        $data = json_encode($request->all());
        $response = Webhook::create([
            'request' => $data 
        ]);
        $param['data'] = 'issue certificate request success';
        return response()->json($param, $this->status_code);
    }
    
    public function manualVerification(Request $request)
    {
        // return redirect('certificate', 308); // 200 OK karena method POST otomatis kebawa

        // return redirect()->to('certificate', 302); // 405 Method Not Allowed karena method POST tidak ikut terbawa

        // success
        $data = json_encode($request->all());
        $response = Webhook::create([
            'request' => $data
        ]);
        $param['data'] = 'manual verification request success';
        return response()->json($param, $this->status_code);
    }


    public function certificate(Request $request)
    {
        $data = json_encode($request->all());
        $response = Webhook::create([
            'request' => $data 
        ]);

        $param['data'] = 'issue certificate request success';
        return response()->json($param, $this->status_code);

    }
    
    public function verification(Request $request)
    {
        $data = json_encode($request->all());

        $response = Webhook::create([
            'request' => $data
        ]);

        $param['data'] = 'manual verification request success';
        return response()->json($param, $this->status_code);

    }
    
    
}
