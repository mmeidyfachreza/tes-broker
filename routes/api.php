<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('issue-certificate', [HomeController::class, 'issueCertificate']);
Route::post('manual-verification', [HomeController::class, 'manualVerification']);

// just redirect route
Route::post('certificate', [HomeController::class, 'certificate']);
Route::post('verification', [HomeController::class, 'verification']);